--1. master look up, Oncotree parent, UMLS, ICDO, ICDO mapped
drop table ot_icdo_umls_snomed;
create table ot_icdo_umls_snomed as 
SELECT distinct a.oncotree_code, a.concept_name as oncotree_name, a.concept_type,an.parent_oncotree_code , e.concept_name as parent_name, 
ncit_code,s.umls_code as orig_umls_Code ,icdo_topography_code,ct.concept_name as icdo_topography_name,
icdo_morphology_code, cm.concept_name as icdo_morphology_name, hemeonc_code,
 c.concept_code as icdo_code, c.concept_name as icdo_name,  b.code as umls_code, b.str as umls_str, d.concept_code, d.concept_name, d.vocabulary_id
FROM mapping_ot s
join concept_ot a on a.oncotree_code = s.oncotree_code
join ancestor_ot an on an.oncotree_code = a.oncotree_code
--not all parent concepts have codes
left join concept_ot e on e.oncotree_code = an.parent_oncotree_code
left join dev_icdo3.concept  ct on icdo_topography_code = ct.concept_code  AND ct.vocabulary_id='ICDO3' 
left join dev_icdo3.concept  cm on icdo_morphology_code = cm.concept_code AND cm.vocabulary_id='ICDO3' 
left join sources.mrconso b on b.cui = s.umls_code and sab ='SNOMEDCT_US'  and suppress ='N' 
and tty ='FN' 
LEFT JOIN dev_icdo3.concept c ON lower(concat(s.icdo_morphology_code,'-',s.icdo_topography_code))=lower(c.concept_code) AND c.vocabulary_id='ICDO3'
left join dev_icdo3.concept_relationship r on c.concept_id = r.concept_id_1 and relationship_id= 'Maps to' and r.invalid_reason is null
left join dev_icdo3.concept d on d.concept_id = r.concept_id_2

--2. splitting by different problems, into different parts

--2.1. Histology and topography filled but there's no resulting ICDO concept (Dima's part)
select * from dev_oncotree.ot_icdo_umls_snomed
where icdo_topography_code is not null  and icdo_morphology_code is not null
and icdo_code is null 
;
--2.2. NCIt code is present but SNOMED- not, Rimma might request to add NCIt as standard, not an oncotree, need to discuss with her, define cases that can't be mapped to OMOP vocabulary amongst these concepts
-- will be one of the latest steps, when the mapping is done
select * from ot_icdo_umls_snomed where ncit_code is not null and umls_code is null
;
--2.3. ICDO resulting mapping and UMLS mapping are different (Eddy's part)
select  * from dev_oncotree.ot_icdo_umls_snomed
where umls_code !=concept_code 
--include only disorders into comparison
and umls_str like '%(disorder)'
;
-- 2.4. there's no mapping to ICDO (Vlad's part)
select * from ot_icdo_umls_snomed where icdo_morphology_code is null and icdo_topography_code is null 
;
--2.3. ICDO resulting mapping and UMLS mapping are different (Eddy's part)
select  * from dev_oncotree.ot_icdo_umls_snomed
where umls_code =concept_code 
;
--2.5 what's left
--what's left after these 3 parts (2.2. is not included)
select * from ot_icdo_umls_snomed where oncotree_code not in (
select oncotree_code from dev_oncotree.ot_icdo_umls_snomed
where icdo_topography_code is not null  and icdo_morphology_code is not null
and icdo_code is null
union all
select oncotree_code from dev_oncotree.ot_icdo_umls_snomed
where umls_code !=concept_code 
--include only disorders into comparison
and umls_str like '%(disorder)'
union all
select oncotree_code from ot_icdo_umls_snomed where icdo_morphology_code is null and icdo_topography_code is null
)
--429 concepts with nmore 
;
--3. manual look ups

--3.1. Was no  mapping to ICDO (Vlad's part)
select * from Oncotree_mapping_for_icd_res


--the look up sent to Rimma it has the full their concepts and mappings when ICDO attributes are not present
select
 b.oncotree_code,b.oncotree_name,b.concept_type,b.parent_oncotree_code,b.parent_name,b.ncit_code,b.orig_umls_code,b.icdo_topography_code,
b.icdo_topography_name,b.icdo_morphology_code,b.icdo_morphology_name,b.hemeonc_code as hemonc_code,b.icdo_code,b.icdo_name,b.umls_code,b.umls_str, 
coalesce (relationship_id_1, 'Maps to (to be checked)') as omop_relationship_id, coalesce (a.concept_code, b.concept_code) as omop_concept_code, 
coalesce (a.concept_name, b.concept_name) as omop_concept_name,coalesce( a.vocabulary_id, b.vocabulary_id) as omop_vocabulary_id
   from ot_icdo_umls_snomed b
  left join Oncotree_mapping_for_icd_res a on  a.oncotree_code =b.oncotree_code